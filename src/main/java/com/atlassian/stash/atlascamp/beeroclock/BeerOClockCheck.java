package com.atlassian.stash.atlascamp.beeroclock;

import com.atlassian.stash.scm.ScmRequest;
import com.atlassian.stash.scm.ScmRequestCheck;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Calendar;

/**
 * An {@link ScmRequestCheck} that blocks all updates to Stash repositories after 4pm on a Friday.
 */
public class BeerOClockCheck implements ScmRequestCheck {

    @Override
    public boolean check(@Nonnull ScmRequest scmRequest) throws IOException {
        if (scmRequest.isWrite() && isAfter4pmOnAFriday()) {
            scmRequest.sendError(
                    "IT'S BEER O'CLOCK",
                    "Your commit access has been temporarily suspended. Have a great weekend!"
            );
            return false;
        }

        return true;
    }

    private boolean isAfter4pmOnAFriday() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY && now.get(Calendar.HOUR_OF_DAY) >= 16;
    }

}
