package com.atlassian.stash.atlascamp.notes;

import com.atlassian.stash.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.StringOutputHandler;
import com.atlassian.utils.process.Watchdog;

import java.io.InputStream;

/**
 * Simple output handler that just returns the output value from the git process, trimmed to null if whitespace only.
 */
public class ShowNoteOutputHandler implements CommandOutputHandler<String> {

    private final StringOutputHandler outputHandler;

    public ShowNoteOutputHandler() {
        this.outputHandler = new StringOutputHandler();
    }

    @Override
    public String getOutput() {
        String output = outputHandler.getOutput();

        // trim to null
        if (output != null && output.trim().isEmpty()) {
            output = null;
        }

        return output;
    }

    @Override
    public void process(InputStream output) throws ProcessException {
        outputHandler.process(output);
    }

    @Override
    public void complete() throws ProcessException {
        outputHandler.complete();
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
        outputHandler.setWatchdog(watchdog);
    }
}
